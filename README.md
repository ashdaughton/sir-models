# Infectious Disease Intervention Analysis (IDIA)
This project includes (1) code to run SIR models with and without simple control measures and (2) an interactive Django visualization of those models.

This is heavily a work in progress.


## Getting Started

In order to use this application you'll need to have postgresql installed. The easiest way to do so is:

1. Install homebrew
2. Use homebrew to install postgresql 
``
brew install postgresql
``
3. Create a [virtual environment](https://docs.python.org/3/library/venv.html)
``
python3 -m venv /path/to/new/virtual/environment
``

4. Activate the virtual environment
5. Install the prerequisites (below)

### Prerequisites

All prerequisites are listed in `requirements.txt`. They can be installed using pip:

``
pip install -r requirements.txt
``

### Running the application

To run the application use:
``
./manage.py runserver
``

Then, visit your localhost url (``localhost:8000``) and play with the tool!


## Built with 

* [Django](https://www.djangoproject.com/) - Web framework used
* [Highcharts](https://www.highcharts.com/) - Used to build interactive charts
* [Plotly](https://plot.ly/) - Used to build interactive charts



## Some notes about parameters

* Initial number susceptible has to be > 2 (range 1000-10,000 is pretty normal)
* Initial number infected has to be greater than 0
* Infectiousness period has to be an integer between 1 and 14
* R0 range has to be greater than 1, but can be a float. Increasingly large ranges cause the server to give out. This is a problem I'm working on.
* Control effectiveness must be between 0 and 1. Floats are acceptable. This describes the fraction of the susceptible population that are removed at each time point (so 0.01-0.1 are realistic values)
* The control start has to be an integer.
* These are also described in popovers on each input. However, they currently only work after the graphs pop up. I'm working on this.

